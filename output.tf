output "host_name" {
 value = "${google_compute_instance.vm_instance.*.name}"
}

output "project_id" {
  value = "${var.project_id}"
}

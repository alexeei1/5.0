variable private_key_path {
}

variable pub_key_path {
}

variable "countvm" {
  default = "3"
}
variable "indx" {
 default = "0"
}

resource "google_compute_instance"  "vm_instance"  {
  count = "${var.countvm}"
  name = "node${count.index+1}"
  machine_type = "e2-small"
  
  connection {
   user = "yanaleshenko3108"
   private_key = "${file(var.private_key_path)}"
   timeout = "2m"
   host = "${self.network_interface.0.access_config.0.nat_ip}"
  }
  provisioner "remote-exec" {
    inline = [
    "sudo apt update",
    "sudo apt install -y nginx",
    "echo ''Juneway' ${self.network_interface.0.network_ip} ' ' ${self.name}'| sudo tee /var/www/html/index.nginx-debian.html"
    ]   
  }
  provisioner "local-exec" {
    command = "echo ${self.name} '  '  ${self.network_interface.0.access_config.0.nat_ip} >> hosts.list"
  }
  boot_disk {
    initialize_params  {
     size = "20"
     image = "debian-cloud/debian-10"
     }
    }
     network_interface {
     network = "default"
     access_config {
    }
   }
}


resource "google_compute_firewall" "http" {
  name = "tcpall"
  network = "default"
  allow {
    protocol = "tcp"
    ports = ["80","443"]
    }
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "udp" {
  name = "udpall"
  network = "default"
  allow {
    protocol = "udp"
    ports = ["10000-20000"]
    }
  source_ranges = ["10.0.0.23"]
}


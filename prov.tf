variable project_id {
}

provider "google" {
  project = var.project_id
  region = "europe-north1"
  zone = "europe-north1-a"
}
